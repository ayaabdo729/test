# test

## To run terraform infra
Change profile to your profile in provider.tf then apply the following:

```
cd infra
terraform init
terraform apply --var-file terraform.tfvars
```

## To run ansible script 
Change ip in the inventory file to your ec2 public ip then:
```
cd ansible
ansible-playbook play-book.yaml --private-key ssh-key.pem -i inventory -e "curl_pkg=curl" -e "ping_pkg=iputils-ping" -e "openjdk_version=openjdk-11-jdk"
```

## Run jenkins as docker container to run the pipeline
```
docker run -d -p 8080:8080 jenkins:jenkins:lts
```
Then configure the ec2 as jenkins agent:
manage jenkins -> manage nodes and clouds -> new node -> fill the needed configuration
Set label as aws

## Create new item jenkins:
Choose pipeline type then add the git repo and configure jenkins path as cronjob/Jenkinsfile
and finally build the pipeline
