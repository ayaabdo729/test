output "http_sg_id" {
  value = aws_security_group.http_sg.id
}

output "https_sg_id" {
  value = aws_security_group.https.id
}

output "ssh_sg_id" {
  value = aws_security_group.ssh.id
}