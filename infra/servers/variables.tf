variable "ssh_sg_id" {
  type = string
}

variable "http_sg_id" {
  type = string
}

variable "https_sg_id" {
  type = string
}

variable "subnet_id" {
  type = string
}

# variable "iam_jenkins_profile" {
#   type = string
# }

variable "ec2_ami" {
  type = string
}

variable "ec2_instance_type" {
  type = string
}

variable "ec2_volume_size" {
  type = number
}
