region                = "us-east-1"


vpc_id                = "vpc-f17eef8c"

vpc_cidr_block        = "172.31.0.0/16"

public_subnet_1a_id   = "subnet-7a7d391c"

public_az_1a          = "172.31.0.0/20"


ec2_ami               = "ami-04505e74c0741db8d"
ec2_instance_type     = "t2.small"
ec2_volume_size       = 50