module "security" {
  source                 = "../security"
  vpc_cidr_block         = var.vpc_cidr_block
  vpc_id                 = var.vpc_id
}

module "servers" {
  source                    = "../servers"
  ssh_sg_id                 = module.security.ssh_sg_id
  http_sg_id                = module.security.http_sg_id
  https_sg_id               = module.security.https_sg_id

  ec2_ami                   = var.ec2_ami
  ec2_instance_type         = var.ec2_instance_type
  subnet_id                 = var.public_subnet_1a_id
  ec2_volume_size           = var.ec2_volume_size

}