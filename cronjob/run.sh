#!/bin/bash

# echo */15 * * * * docker run -d -v /opt:/opt -v /var/run/docker.sock:/var/run/docker.sock ${REPO}:${TAG} >> /etc/crontab

crontab <<EOF
*/15 * * * * docker run -d -v /opt:/opt ${REPO}:${TAG}
EOF
