#!/bin/bash

# top | awk '{print $1, $2}'

# atop -w cpu
# ps  -A -o pid,%mem  | awk '{print $1","$2}' >> mem.csv
# # df -Th | awk '{print $1","$4}' >> disk.csv
# df -Th | awk '{if (NR!=1) {print $1","$4}}' >> disk.csv
sudo chmod 777 /opt
ps  -m -o pid,%cpu,time | awk '{print $1","$2","$3}' >> /opt/CPU.csv
free | grep "Mem" | awk '{print $4/$2 * 100","strftime()}' >> /opt/MEM.csv
df -h | grep -w "/" | awk '{print $4","strftime()}' >> /opt/DISK.csv
# monitor